package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.course.Course;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.course.CourseRepository;



@CrossOrigin(origins = { "http://localhost:3000"})
@RequestMapping("/api/v1/")
@RestController
public class InstructorResource {

	@Autowired
	private InstructorRepository instructorRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
		
	@GetMapping("/instructors")  
	public ResponseEntity<List<Instructor>> getAllInstructors(){
		try {
			List<Instructor> instructors = new ArrayList<Instructor>();
			instructorRepository.findAll().forEach(instructors::add);
			if(instructors.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(instructors, HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GetMapping("/instructors/{id}")
	public ResponseEntity<Instructor> getInstructor(@PathVariable long id) {
	 Optional<Instructor> instructorData = instructorRepository.findById(id);
	 
	 if(instructorData.isPresent()) {
		 return new ResponseEntity<>(instructorData.get(), HttpStatus.OK);
	 }else {
		 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	 }
	}
	
	@PutMapping("/instructors/{id}")
	  public ResponseEntity<Instructor> updateInstructor(@PathVariable long id,
	      @RequestBody Instructor instructor) {
	
		Optional<Instructor> instructorData = instructorRepository.findById(id);
		Instructor _instructorData = instructorData.get();
		_instructorData.setUsername(instructor.getUsername());
		_instructorData.setNombre(instructor.getNombre());
		_instructorData.setApellido(instructor.getApellido());
	
	    return new ResponseEntity<>(instructorRepository.save(_instructorData), HttpStatus.OK);
	}
	
	  @PostMapping("/instructors")
	  public ResponseEntity<Instructor> createInstructor(@RequestBody Instructor instructor) {
		  
	    try {
	    	Instructor _instructor = instructorRepository.save(new Instructor(instructor.getUsername(), instructor.getNombre(), instructor.getApellido()));
	    	return new ResponseEntity<>(_instructor, HttpStatus.CREATED);
	    	
	    }catch(Exception e){
	    	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	    
	  }
	  
	
	  
	  @DeleteMapping("/instructors/{id}")
      public ResponseEntity<Void> deleteInstructor(@PathVariable long id){
		  
         try { 
        	 
        	 Optional<Instructor> instructor = instructorRepository.findById(id);
        	 List<Course> courses = new ArrayList<Course>();
        	 
        	 
	      	 	if(instructor.isPresent()){
	      	 		Instructor _instructor = instructor.get();
	      	 		courseRepository.findByUsername(_instructor.getUsername()).forEach(courses::add);
	      		    _instructor = instructor.get();
	      		}	 	        	 
        			 
	      	 	for(Course curso : courses) {	 
        			 courseRepository.deleteById(curso.getId());       		 
	      	 	}
        	 
        	 instructorRepository.deleteById(id);
        	 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
      } 
	    
}
	

