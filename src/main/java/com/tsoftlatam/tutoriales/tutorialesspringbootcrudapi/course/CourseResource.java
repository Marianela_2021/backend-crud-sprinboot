package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.course;

//import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.DTO.CoursesDTO;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructor.Instructor;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructor.InstructorRepository;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
public class CourseResource {
	
	//@Autowired  
	//private CoursesHardcodedService courseManagementService;  
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private InstructorRepository instructorRepository;
	
		
	@GetMapping("/instructors/{username}/courses")  
	public ResponseEntity<List<CoursesDTO>> getAllCourses(@PathVariable String username){
		try {
			List<Course> courses = new ArrayList<Course>();
			List<CoursesDTO> coursesDTO = new ArrayList<CoursesDTO>();
			//Instructor instructorById = new Instructor();
			courseRepository.findAll().forEach(courses::add);
			
			for(Course curso: courses) {
				Instructor InstructorTemp = null;
				
				if(curso.getUsername() != null || curso.getUsername() != "" ) {
                    InstructorTemp =  instructorRepository.findByUsername(curso.getUsername());
                }
				
				if(InstructorTemp != null) {
					//instructorById = InstructorTemp.getId();
                    coursesDTO.add(new CoursesDTO(
                            curso.getId(),
                            curso.getDescription(),
                            InstructorTemp.getUsername(),
                            InstructorTemp.getNombre() + " " + InstructorTemp.getApellido()));
                } else {
                	InstructorTemp = null;
                    coursesDTO.add(new CoursesDTO(
                            curso.getId(),
                            curso.getDescription(),
                            "Unassigned",
                            "Unassigned"));
                   
                }
            }
                                   
            if(coursesDTO.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(coursesDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
	}
			
			
			
			
	
	
	@GetMapping("/instructors/{username}/courses/{id}")
	public ResponseEntity<Course> getCourse(@PathVariable String username, @PathVariable long id) {
	 Optional<Course> courseData = courseRepository.findById(id);
	 
	 if(courseData.isPresent()) {
		 return new ResponseEntity<>(courseData.get(), HttpStatus.OK);
	 }else {
		 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	 }
	}
	
	@PutMapping("/instructors/{username}/courses/{id}")
	  public ResponseEntity<Course> updateCourse(@PathVariable String username, @PathVariable long id,
	      @RequestBody Course course) {
	
		Optional<Course> courseData = courseRepository.findById(id);
		Course _courseData = courseData.get();
		_courseData.setUsername(course.getUsername());
		_courseData.setDescription(course.getDescription());
	
	    return new ResponseEntity<>(courseRepository.save(_courseData), HttpStatus.OK);
	}
	
	  @PostMapping("/instructors/{username}/courses")
	  public ResponseEntity<Course> createCourse(@PathVariable String username, @RequestBody Course course) {

	    try {
	    	Course _course = courseRepository.save(new Course(course.getUsername(), course.getDescription()));
	    	return new ResponseEntity<>(_course, HttpStatus.CREATED);
	    	
	    }catch(Exception e){
	    	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	    
	  }
	  
	
	  
	  @DeleteMapping("/instructors/{username}/courses/{id}")
      public ResponseEntity<Void> deleteCourse(@PathVariable String username, @PathVariable long id) {

         try {
            courseRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
      }
	  
	  
	  @GetMapping("instructors/{username}/coursesByUser")
	  public ResponseEntity<List<Course>> getCoursesByUser(@PathVariable String username){
	  try {
		  
		  List<Course> courses = new ArrayList<Course>();
		  courseRepository.findByUsername(username).forEach(courses::add);
		  if(courses.isEmpty()) {
			  return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		  }
		  
		  return new ResponseEntity<>(courses, HttpStatus.OK);
		  
	  }catch(Exception e) {
		  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	  
	  }
}
	
	
