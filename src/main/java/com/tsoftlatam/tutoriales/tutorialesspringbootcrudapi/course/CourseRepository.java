package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.course;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	List<Course> findByUsername(String username);
	
	//List<Course> findByCourseContaining(String description);
}
